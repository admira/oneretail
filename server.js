var express = require('express');
var cookieParser = require('cookie-parser');
var querystring = require('querystring');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var readline = require('readline');
var request = require('request');




app.use('/server_code', express.static(__dirname +  '/server_code'));
app.use('/login', express.static(__dirname +  '/login'));

// need cookieParser middleware before we can do anything with cookies
app.use(cookieParser());
function checkCookies (req, res, next) {
  // check if client sent cookie
  var name = req.cookies.name;
  var age = req.cookies.age;
  var sex = req.cookies.sex;
  //if (name === undefined || age === undefined || sex === undefined || sex2 === undefined)
  if (name === undefined || age === undefined || sex === undefined )
  {
    console.log('redirect');
    res.redirect('/login');
  } 
  else
  {
    // yes, cookie was already present 
    console.log('cookie exists', name);
    next();
  } 
   
};

app.get ('/login/new/:name/:age/:sex', function(req, res) {
    var randomNumber=Math.random().toString();
    randomNumber=randomNumber.substring(2,randomNumber.length);
    res.cookie('name',req.params.name, { maxAge: 900000, httpOnly: true });
    res.cookie('age',req.params.age, { maxAge: 900000, httpOnly: true });
    res.cookie('sex',req.params.sex, { maxAge: 900000, httpOnly: true });
    res.cookie('rnd',randomNumber, { maxAge: 900000, httpOnly: true });
    console.log('cookie created successfully');

    /*res.redirect (
        'file:///Users/jserret/programming/iot/oneretail/server_code/loginSucces.html?' +
        '&name=' + req.params.name +
        '&userId=' + randomNumber +
        '&age=' + req.params.age +
        '&sex=' + req.params.sex
    );*/

    res.redirect (
        'http://pwr.admira.com/loginSucces.html?' +
        'userId=' + randomNumber +
        '&name=' + req.params.name +
        '&age=' + req.params.age +
        '&sex=' + req.params.sex
    );
    //res.redirect('/login/succesfull');
});

app.get('/screen/:id', checkCookies, function(req, res) {
    console.log(req.params)
    condition ={
        'screenId' : req.params.id,
        'name' : req.cookies.name,
        'age'  : req.cookies.age,
        'sex'  : req.cookies.sex
    } 
    for (var socket in activeSockets) {
            activeSockets[socket].socket.emit("condition", condition);
    }

    //res.send('Hello ' + req.cookies.name + '. Wellcome to ' + req.params.id + '. Your age: ' + req.cookies.age + '. Your sex: ' + req.cookies.sex) ;
    res.redirect (
        'http://pwr.admira.com/screen.html?' +
        'screenId=' + req.params.id +
        '&userId=' + req.cookies.rnd +
        '&name=' + req.cookies.name +
        '&age=' + req.cookies.age +
        '&sex=' + req.cookies.sex
    );
});

var activeSockets = [];
io.on('connection', function (socket) {

    activeSockets[socket.id] = {socket: socket, added: new Date()};
    console.log('a player connected');

    socket.on('disconnect', function () {
        delete activeSockets[socket.id];
    });


});


http.listen(3000, function () {
    console.log('listening on *:3000');
});
