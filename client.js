
var ioClient = require('socket.io-client');
var fs = require('fs');

//console.log("Agent Serial Arduino v1.0.0 - stable")

//var admiraBasePath = 'C:/ADmiraPlayer/Admira'
var admiraBasePath = 'C:/admira/'

var server = {
	ip : '192.168.1.154',
	port : 3000
}

var socket = ioClient.connect('http://' + server.ip + ':' + server.port,  {reconnect: true});

  socket.on ('connect', function(){
  	console.log('Ready')
  })

  socket.on('condition', function (condition) {
    console.log(condition);

    writeContent ( 'info', condition ) //TODO

    sendCondition ( 'pressure', 31, condition.screenId)


    socket.emit('conditionLaunched', {});
  });

  socket.on ('error', function(err){
    console.log('err')
  })

  socket.on ('disconnect', function(err){
    console.log('err')
  })

function sendCondition ( name, id, value){
    console.log ( 'Condition fired for : ' + tag );
    var now = new Date();
    tstamp = Math.floor( now.getTime() / 1000 );
    fs.writeFile( admiraBasePath + '/conditions/' + name + '.xml',
                                    '<?xml version="1.0" encoding="UTF-8"?><condition id="' +
                                    id +
                                    '" tstamp="' +
                                    tstamp + 
                                    '" version="1.0" status="ok" ><value>' + 
                                    tag + 
                                    '</value><info> Service OK</info></condition>',
        function(err){
            if(err){
                console.log('Writing analytics condition data error: ' + err);
            }else{
                console.log(tag);
            }
        }
    );
}


function writeContent ( name, content){
    console.log ( 'Content writing for : ' + content );
    fs.writeFile( admiraBasePath + '/content/' + name + '.json', JSON.stringify (content),
        function(err){
            if(err){
                console.log('Writing content data error: ' + err);
            }else{
                console.log(content);
            }
        }
    );
}



function sendStats ( tag ){
    console.log ( 'Stats fired for : ' + tag );
    var now = new Date();
    tstamp = Math.floor( now.getTime() / 1000 );
    var aux = {
         'device': 00,
         'type': 'kimaldi',
         'tstamp': tstamp,
         'data': [ {'reader' : 01 }, {'tag' : tag }, { 'average' : 0 } ]
     }
    fs.writeFile( admiraBasePath + '/modules/silo/object_' + tag + '_' + tstamp + '.json', JSON.stringify (aux),
		function(err){
			if(err){
			    console.log('Writing stats data error: ' + err);
			}else{
			    console.log(tag);
			}
		}
    );
}
